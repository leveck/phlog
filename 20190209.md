# Apple Newton eMate 300 Battery Mod Complete
### Entered on Apple Newton eMate 300
### 20190209

I successfully installed modern Duracell AA NiMH 
rechargeable batteries in my Apple Newton eMate 300 today, 
9 February 2019. Another cool thing is that I can actually 
see washers under the screws for the hinges through the 
translucent plastic of the case.  No need to disassemble the 
screen assembly to check it out now. Feeling thankful for 
that!

One project done, a few more to go...
