# Life is good

*Entered: in emacs on GPD Pocket X7* |
*Date: 20221017* |
*Soundtrack: Grateful Dead - Blues for Allah*

## Merrily, merrily, merrily

...living the dream.

So, things have been pretty good. Been all over the US as
of late, as usual. I have been ensuring that I have
sufficient home time. Winterized the house last weekend
after waking to find frost on my car.

Days pass and I have nothing really worth sharing to say.
So I'll write about that media player I recently bought.

## Hisense Touch Music Player

This player is pretty damned nice. It runs Android 11 and
even though it is supposed to have a nice DAC, I have only
used it so far with bluetooth headphones. It works with
my seagate USB SSD which made it easy to load my music
library onto the device. I have the kindle app loaded on
it and have been reading on it while listening to music
while traveling. The battery lasts an entire travel day
and I have only seen it down to 52% so far. I have yet
to see how long it truly lasts. The practical result is:
long enough.

## Project free

I am blissfully project free at the moment. No music. No
software. No hardware. No sewing. Nothing.

I have been devoting my time instead to reading and vintage
computer videos when out of town, and family time when home.
I have had thoughts of switching to a PDF planner so I can
use my Onyx Boox Nova C to handwrite onto it and save to the
cloud. I have had other transient ideas as well, but just
have not felt the need to move forward with any of them. We
will see if this continues.
