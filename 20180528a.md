# On bicycles
### -=20180528=-

I have been thinking of purchasing a bicycle. I have not owned one for over 15 years. The inspiration came from people I see riding around, and also the phlog of Sparcipx[0], who's writing of his love of bicycle riding is infectious.

I envision riding out of downtown and onto the highway (one lane each direction, non divided) and out to some two track roads. Early morning strikes me as the time I would like to do this. I could bring my mobile RPG setup and find a place in the shade to hang out for a bit.

This needs to happen...

-|-

* [0][gopher://sdf.org/1/users/sparcipx/phlog/](gopher://sdf.org/1/users/sparcipx/phlog/)
