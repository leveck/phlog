# On Camping
### Entered on Apple Newton eMate
### (edited on HP OmniBook 800ct in MS Word 97)
### 20190731

Just got back from Ashley National Forest in Utah. My wife,
youngest daughter, and I spent three days and two nights 
camping in the woods. We slept in a tent and cooked on a 
fire. Oddly enough, this was the first time I have ever 
went camping. It is definitely something we're going to be 
doing more of.

I have a list of things to do differently. A thin mat under 
a warm weather sleeping bag is hard on a forty-something 
year old back. I am thinking of giving hammocks a try.
Also, while my girls liked swimming in Flaming Gorge, it 
left me shivering with purple lips... Some things we over
packed, other things, such as a spatula, we forgot 
altogether (imagine scrambling eggs in a cast iron skillet 
over a camp fire using a telescoping hot dog fork). A great 
time was had, many mad-libs were played, many memories were 
made.

My vacation continues now at home for the rest of the week. 
Loving every minute of this.
