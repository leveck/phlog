# Phlog Rewrite - Part the Next
### -=20180312=-


I took a page from tomasino's book[0] and changed my phosts to type one. Adding all the requisite RFC1436 fields. So far I have only bothered to add links to phlogs >= 20180308, since this represents the format change. There are other changes behind the scenes. I am now using Aspell[1] to stop myself from looking illiterate due to a fat finger. Changing type to 0 and adding the directory typezero behind the file name will allow the plain-text version. I may auto-link this in the future. I am maintaining two distinct copies to facilitate quick atom feed generation. The type one copy is what I am considering canonical. The SDF copy has and shall remain, self referencing. In other words, there is a complete copy on SDF of this phlog, not just a gophermap pointing to RPoD. This is to assuage my paranoia. Just in case the copy on RPoD's external drive, the copy on RPoJ, and the copy on GitLab all vanish, well at least I have the copy on SDF. I think this level of precaution makes a
paper journal seem to have a tenuous chance of remaining in existence.

This will be the last meta phost for a bit. Who wants to read a phlog about making a phlog? In other news, still waiting for my troff book. I ordered the O'reilly classic Sed & Awk as well. Days later. I expect to see it first. Prime shipping is the sole reason I haven't been able to make myself drop Amazon. They're evil as hell, but damn are they good at what they do.

-|-

* [0][gopher://gopher.black/1/phlog/20180311-motsognir-and-burrow](gopher://gopher.black/1/phlog/20180311-motsognir-and-burrow)
* [1][gopher://gopherpedia.com/0/GNU%20Aspell](gopher://gopherpedia.com/0/GNU%20Aspell)
