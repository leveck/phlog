# On safety

*Entered: in vim on gemini pda* |
*Date: 20210807*

I have been ill at ease with "safety culture" for nigh on 20 years.
It never sat right with me. Not just because corporations do not truly
care about us, that point is obvious - they only have profit motive.
The underlying items that generated my feeling are: cowardice,
emasculation, and subjugation.

## Cowardice

Heroes and heroines of the past did not have a *safety culture*. They
did not "play it safe." Kingdoms, empires, countries, and nations did
not hold that *safety* was a virtue; that we should, as is often
articulated in modern corporate safety videos and literature, "be
selfish, do it for your self - your safety is your number one
priority." The old world was carved by risk takers. Risking ones'
safety in the pursuit of a goal, or in the preservation of others
was seen as virtuous.

You may be thinking to yourself, "well, soldiers, law enforcement,
and even medical professionals still do this." No. You are wrong.
The culture of safety worship has wrapped its tendrils about these
vocations as well. These are not the respective workplaces of
yesteryear.

It is cowardly to eschew all risk. It is cowardly to selfishly only
think of your own safety. This has been the stance of great cultures
throughout the ages - the Greeks, Romans, Germans, Britons, etc.

## Emasculation

It is feminine to avoid risk. A requirement of modern employment is
to be forced to relinquish a portion of your masculinity. Society
requires both yin and yang. The Chinese acknowledged this, just as
the Hermeticists, and just about every other classical spirituality
and philosophy did as well. The modern west, however, has decided
to abandon masculinity altogether and morph into an extreme unbalanced
feminine only hellscape. Men are to become poor facsimiles of women
and women into feminized mimeographs of men. Men are encouraged to
behave in very feminine ways, to be overly emotional, to prioritize
their own well-being instead of forming and taking care of a family,
to stymie all fight instinct. While women are encouraged to disdain
all outward feminine traits: to despise bearing children, to pursue
careers outside the home, to dominate traditionally male fields.
All the while pushing emotionalism and feminine thinking as the only
option in society.

## Subjugation

The masses have been enslaved with no recourse into the only mould
available them. OSHA, and the analog in all western nations are all
enforcement arms of correct think. You may be thinking, well these
arms of government make the work place more tolerable, make employers
less able to exploit us, and yes increase safety.

I maintain that they cross the line substantially beyond any benefit
they provide. The maze of government regulation greatly resembles
religion in the deep study and written interpretation that surrounds
it. Once again, this ultimately is not about your safety or freedom
from exploitation at all. It is all directly associated with profit
motive. You as a human are reduced to an entity that will accept the
dictates with no freedom to decide for yourself what risk is
acceptable to you, or what methods you take to determine that.

To rationalize this as the good of the collective needs prioritizing
is once again dehumanizing and radically feminine. We are not bees
or ants. Huge worksites are themselves unnatural and another problem
in and of themselves.

* Everyone is a victim when no one has to fight for what they get.
* Everyone is a coward when no risk is taken.
* Eliminating masculinity in favor of an unbalanced society hurts men
and women.
