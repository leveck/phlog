# On what is modern

*Entered: in Alphaword on Alphasmart Dana* |
*Date: 20200131*

In my mind, things that are *modern* start around 1992. This goes
for computers, music, movies, books, whathaveyou. I typically stop
collecting "classic" band's albums at this delineation. I am much
less interested in, for example, operating systems by Microsoft,
post-1992.

I did not have an 8bit computer as a kid. My first computer was an
IBM XT. This possessed an Intel 8088 running at 4.77MHz, and had
a 1MB RAM expansion and a 20MB MFM hard disk. After this came a
Macintosh SE and a Macintosh LCII. The LCII is what I used right up
until the afore mentioned *modern* era.

My maternal grandfather, who passed away in 1990, was a smart dude.
He held a few tech industry jobs back in the day. He kicked himself
for not investing in Apple when the Steves were in their garage. He
worked at GTE on databases. He worked at IBM and is named on a couple
of patents in that era (early 1970s). He is the person who gave my my
first computer in 1987 when my family returned to the United States
from West Germany. A couple of years later, he gave me the Mac SE.
The cool thing about this, besides receiving a couple of computers,
it they came loaded with dozens of diskettes of development tools.

On the IBM XT, I had the BASICA compiler, the Borland C and Pascal
compilers, and a host of other nifty things such as 4DOS. DOS 2.11
was what the machine came to me running, but I later upgraded it to
DOS 3.33. On the Mac, I had a number of development tools, however,
I fell in love with Hypercard. I used things such as the Apple
Resource Editor to strip sound and graphics resources out of other
programs (such as SIM Earth that my parents bought me), and added
those resources to Hypercard stacks. My first music recordings were
scripted in Hypercard and recorded to cassette tape (first left
channel, then manually synced to the right channel as the Mac SE and
LCII outputted mono).

Post 1992 really was the modern era. In 1993, I got my first ISP
account. I had dial up via a Hayes external 2400bps modem. This was
on the Macintosh LCII. I bought a book called something like "The
Macintosh Internet Kit." It came with floppy disks that had Mosaic
(or some other browser of the era for Mac?), TurboGopher, Archie
and Wais clients, and lots of documentation. The web was much
smaller, and back then it was perfect. So perfect, in fact, that I
really had no interest in Gopher back then. I tried it out numerous
times, but my WWW browser could display gifs (jpegs were opened in
an external program to decompress and display them, and were less
cool). It took my 2400bps modem one hour to download 1MB. I did not
encounter this on many occasions. Most of the web back then was
very simple HTML. and small image files. Downloading software was
time consuming, and tied up the family telephone line.

In 1996 or so I got a 486dx4100. Well, first I picked up a 286
motherboard someone tossed, then a 386, then the 486... it may have
been 1997 or so for the 486. When I had the 486, at first I did not
have a VGA monitor. So I ran Windows 3.11, and later Windows 95
with a CGA screen. In this configuration, Windows used the CGA high
resolution mode, which was 2 color. I had acquired a 14.4kbps modem
card around this time. I used Internet Explorer 2 with that CGA
screen, and also lynx and at that time would dial into the Rio Grande
Freenet. The RGF was a free use Unix system and ISP in El Paso,
Texas that was ran by the local community college. My first personal
website was made during this time. Eventually I acquired a used VGA
monitor, pretty sure all of this equipment in the time line was free.

In 1998, at 22 I graduated from college and got a job as a
programmer. At this time I was provided with a dual channel (128k)
ISDN line to my apartment. This was the time when I first installed
Linux. I bought a big box copy of Red Hat at a large electronics
store. I had a block of static IP addresses from my ISP and a high
speed line. This was my first self hosting. Back then hosting email
was easy. I had a website and a mail server. This all still feels
very *modern* to me. It is a stark contrast to the *before times* of
16 bit machines. Downloads were peppy. Streaming video was present.
I was an adult and purchased this hardware on my own. Dinking around
on a computer was still fun, but at that point it was beginning to be
very tied to having a network.

That may be the demarcation right there in a nutshell, and I do not
think I am alone in recognizing it. The classic period for me from
the 80s to 1992 was offline. I'd dial into local BBS's from time
to time but it was a treat, and there was a time limit to be on.
I would spend my time writing code, or playing games, or writing
stories, or scripting music. I didn't care about the news, or other
people's status messages. I listened to music on cassette tape or
on the radio. It was a time that feels much more separate. I feel I
was a stand alone being. And yet it did not feel like isolation. I
socialized in school, rode my skateboard in the neighborhood, there
were plenty of things to do. Also, in 1992, I moved from El Paso to
southern New Mexico. There were no paved roads. I was old enough to
drive. Shortly after that the internet became a thing for me. This
likely makes the dividing line all the more engraved for me.

The world as a whole has changed drastically. There are points of
reference for me here. I still hold 1992 was the end of an era. 2001
was another. The old world died that year, in September. 2012 was the
birth of the current existence. That even takes a different turn in
2016. I feel we have lost a lot. The world was not a safe place in
the past, but it was better. Eschewing all oldness just because it is
old neglects the fact that it was built on, in some cases, hundreds
or thousands of years of common sense. If people have to be forced
to go along with a change then it likely should not be changed. Like
most people, I just want to be left alone, to make my own way, and
not be scrutinized, subjected to needless rules, or have the rights I
was born with whittled away. This was all so very much easier before
the network.

The internet has a [meme that the timeline has changed][1]. I do not
know if I buy into a lot of that, but the common years given as shifts
coincide with my own observations. I am not alone in how I feel. I am
not alone in cheering pandemics on, or wishing for a global collapse.
It will right what was wronged. I just hope I am alive to see it.

[1]: gopher://1436.ninja:70/0/Document_Store/1580588995.txt

