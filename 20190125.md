# On phlog revisions and the small internet (RE: visiblink)
### Entered in vim on RPoJ via ssh from iPad w/ external keyboard
### 20190125

In his latest [phost][1], visiblink puts out a request for
the prevailing take on revising phlog entries. Here is my
$0.02 -- I typically allow myself to make revisions for about 
the first hour. These are limited to formatting and spelling
corrections. After that, the phost lives its permanent life.
If I particularly regret something I have written, the 
entire phost gets deleted. If it is something that has taken
on a life of its own, then deleting would not be honest. In
this case a follow up entry explaining what I would have
edited is in order.

IMHO, this approach is in keeping with my personal 
philosophy and with that of the _small internet_ as espoused
elsewhere. I just want to add, that I do whole-heartedly 
agree with the notion of the small internet as put forth
by _spring_, et al. I would like to draw a distinction 
between scraping content like LX'ers into gopherspace, and
creating interfaces to things like Wikipedia. One is 
bringing a foreign community in without our participation,
and the other is a front-end to raw information. Maybe this
is just to rationalize my creation of the Gutenberg front-end
but it seems different. 

The part where I am torn is that I used the LX'ers front-end,
and I use the Hacker News gopher front-end more than the WWW
site. On most world wide web sites, I lurk instead of 
participate. So scraped content is the same for me as being 
in a web browser. On hacker news, for example, I have 
commented only once, a calculated comment to raise my karma
to exactly 2 so I could link it to keybase. Back in the 90s
I had dozens of troll accounts on /. -- then the novelty
of participating in a large discussion forum wore off.

I think, after reading what I have written, I am advocating
for a middle ground. I do not want to have :70 turned into
a religion. In the end, we are a collective of independent 
servers who do what we wish. This is important. More so
than the feeling of any one of us. The platform excludes
most badness by its tech -- no JavaScript, no cookies, no
popups, no redirects... content is avoidable by decision.
On gopher there is room for communists, Nazis, incels, 
trans, even bronies. I do not have to visit what I do not
like, and this extends to scraped content. 

[1]: gopher://zaibatsu.circumlunar.space/0/~visiblink/phlog/20190124 "On the Question of Phlog Revisions"
