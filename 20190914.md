# On the 90 percent of gopher...
### entered in vim on RPoJ
### 20190914

There is the 90% of gopher that I am
not familiar with. All the gopherholes
out there I have never surfed to, that
I do not link to. So, to rectify this,
I have been actively surfing. 
Following links on sites I come across
finding yet more sites. The floodgap
server list is of course a good 
start for this activity, but you will
find servers not listed there by 
following links from sites you do find
there. 

There are so many colonies. Cliques of
persons who mutually link to each 
other, reply to each other's posts,
and seem to comprise the gopherspace
in its entirety for that clique. 
Other groupings seem more or less 
aware of the larger personalities in
gopherspace stemming from large
colonies such as the SDF. Sometimes
the division is along language lines
-- there is a large number of 
gopherholes in German, for example,
and another grouping in French. 

I have gotten some ideas for RPoD
today, while surfing. The idea of
possibly creating another gopherhole
for a given purpose, hosted on a 
different port number of 1436.ninja.
I may do this as an experiment and
use the [RPoD General Purpose Section](http://leveck.us/Phlog/gen/)
of the website as the guinae pig.
I have been planning on writing a 
cgi-wrapper for the RPoD-GPS for as
long as it has existed. In a like
fashion to how my Phlog works on
gopher: write in markdown and an
intermediate cgi app presents 
clickable links leaving the post.
I am using apache_mod_hoedown for
the RPoD-GPS, specifically to have 
footnotes so my wrapper will need
to correctly present these.

Anyways, I know a lot of you do 
actively surf gopher looking for new
servers. I have done this on and off 
over the years, but have not in a 
while. It was over due. I also need 
to go further down the <strike>rabbit</strike>
gopher hole on a lot of servers that
have large content collections. I 
have found that floodgap does not 
seem to return to a server after it
spiders (sometimes?). For example,
1436.ninja, leveck.us, gopher.leveck.us,
and rpod.leveck.us all point to RPoD.
Floodgap came accross all these 
domains at different points in my
gopherhole's existence. It reports
each as having a different number of 
selectors (when they are all the same).
I wonder if it only recording selectors
for a (sub)domain where the link
leading to that subdomain utilizes it.
I do not have any idea. I toyed with
running a veronica instance, but it 
is a huge resource sink. I ended up
not keeping it active for very long.
Maybe a dedicated Raspberry Pi with
a 256GB thumb-drive as storage would
be  way to do such a thing. I imagine
that that amount of storage would be 
sufficient for my lifetime (and be
largely empty), unless we get some 
rather large binary-hosting gopher-
holes running. RPoD runs on household
DSL in rural Wyoming, and it is not 
super fast. My family would probably
not like the bandwidth hit...

But I digress. The point was that if 
you do not surf gopher beyond the 
phlogs, you are missing out. The chans
(I would never usually use that phrase,
but these are hardly image boards) are
another fun time if you are familiar
with board culture. I know of three,
but there are likely more. There are 
a LOT of www-scrapers out there. Which
I am not conceptually a huge fan of
(if you ask me), but if you actually
watch me, I use the crap out of them.
I lurk on sites. I am not a commenter.
So, having a text-only, light weight,
ad-free gopher version of HN, or /.,
or Lobsters, or what have you, is 
pretty damned handy. There are apps
galore: recipes, cocktails, weather,
games (text-based), searches of 
various things, you name it... (note
to self -- it has been a while since
I wrote a new gopher-fronted-something
maybe it is time to make something). 

Gopher is the girl you marry. She is 
not flashy, but is beautiful to you.
She holds up her end of the 
relationship. Gopher will not leave 
you, won't be unfaithful, won't
embarrass you in public. As the years
fly by, your love for gopher only 
becomes stronger.

Yeah, the cringe quotient of RPoD was
lacking, so I cranked it up a notch.
Don't act like you disliked that...
