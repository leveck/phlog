# Hey, sysdharma - There are feeds on 1436

### -=20180318=-

sysdharma writes[0] about an interesting project idea for a decentralized gopherd using IPFS. Then states that it would be cool if gopher had RSS.

The following gopherholes have feeds:

* RPoD Phlog[1]
* tx.god.jp[2]
* bitreich[3]
* ulcer[4]

I am sure I am missing some... Just pointing out that there are feeds on gopher. It would be cool if more people generated them. A cool project may be the push people need.

-|-

* [0][gopher://sdf.org/1/users/sysdharma/phlog/2018.03.18](gopher://sdf.org/1/users/sysdharma/phlog/2018.03.18)
* [1][gopher://gopher.leveck.us/1/phlog](gopher://gopher.leveck.us/1/phlog)
* [2][gopher://tx.god.jp](gopher://tx.god.jp)
* [3][gopher://bitreich.org](gopher://bitreich.org)
* [4][gopher://sdf.org/0/users/ulcer](gopher://sdf.org/0/users/ulcer)
