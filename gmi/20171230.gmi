# On comments

Recently there has been a few posts on commenting on phlogs, and in general from Logout[0], Ulcer[1], Tomasino[2], Papa[3], Slugmax[4], probably more.... Here is my $0.02: I agree with the anti direct comment contengent on this one.

I have been doing some thinking of late after it was pointed out to me that I have tended to bring www-tinged ideologies and baggage with me as I wade thru gopherspace. Comments really are a webism, and an abuse of a system setup for the listing and displaying of files and directories. By the same token, the facility for implementing comments exists of course in the form of type 7. it has been done by many persons already.

Replying with a phlog entry is much more personal, engaging, and yes time consuming. I know I feel a certain excitement when I am referenced in someone else's phlog and I appreciate the time that other people have taken to do so. In the end it is much more gopherish to reply in this manner, and the result is more thoughtful and tends to build a tighter community.

* [0][gopher://i-logout.cz/0/en/phlog/12-2017-phlog.txt](gopher://i-logout.cz/0/en/phlog/12-2017-phlog.txt)
* [1][gopher://sdf.org/1/users/ulcer//cgi-bin/godot-post%3f171219_2028](gopher://sdf.org/1/users/ulcer//cgi-bin/godot-post%3f171219_2028)
* [2][gopher://sdf.org/1//users/tomasino/phlog/20171218-comments](gopher://sdf.org/1//users/tomasino/phlog/20171218-comments)
* [3][gopher://grex.org/0/%7epapa/pgphlog/ac7-tfurrows_s_8088](gopher://grex.org/0/%7epapa/pgphlog/ac7-tfurrows_s_8088)
* [4][gopher://sdf.org/0/users/slugmax/phlog/reply-to-sysdharma](gopher://sdf.org/0/users/slugmax/phlog/reply-to-sysdharma)
