# RE: Jirka, Palm OS questions

### Entered in vim on RPoJ via ssh from iPad

### 20190922

Jirka writes
=> gopher://sdf.org/0/users/jirka/Phlog/2019_09_13.txt ^1
of the Tungsten W and the  charging cable. The Tungstens, m130, and others  of that era used the Palm Universal Connector
=> gopher://gopherpedia.com/0/Palm%20Universal%20Connector ^2
. These are plentiful and cheap on eBay!

On T|C/W cases: I have the OEM Tungsten case by Palm, but I hate it. The "leather" is cheap and it is already falling apart even though it was new old stock. I should be getting  the Belkin Leather Flip case on Monday. I will probably write in my twitpher what I think of it. I have the Belkin Leather Flip Case for the Treo 90/180 and I love that case.

Jirka also writes
=> gopher://sdf.org/0/users/jirka/Phlog/2019_09_14.txt ^3
 of seeking opinions on the Palm IIIc. I never had that one, but I had  coworkers that did at the time and I remember that device fondly. The screen was not known for good performance in direct sunlight. But, neither is my UX50 or my Treo 90. Use while at the grocery store will be just fine for the IIIc, however. Using it with the PalmPix will also be fine if you cup your hands around the screen. The batteries are of course readily  available still online, and I would assume that any unit you can buy would pretty much need its battery swapped out.
