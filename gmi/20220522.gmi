# Another Project update

Entered: in vim on gemini pda | Date: 20220518

## Adventures in additional protocols

Added a function that calls 
=> https://github.com/aaronjanse/gcat gcat
then parses gemini in a similar manner to the way I handled gopher. Also added a variable called http that you can set to your http(s) browser (defaults to lynx). So now this little script handles gopher/gemini/http(s).

I included gcat and a copy of its license in the bashgopher repo.

Anyways, find it here: 
=> https://gitlab.com/leveck/bashgopher 
=> https://gitlab.com/leveck/bashgopher
