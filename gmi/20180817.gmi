# On Work

### 20180817

It seems like common knowledge that the death knell of a business is when the accountants run the company. Psychopathy runs rapant and numbers are the only thing that matters. Not equipment, and definately not the employees.

Watching the company I work for progress down this path over the last 14 years is eerie. It feels foreign and very unwelcome. The next 21 years until retirement wil be tense. Starting over again is a very stressful thought. I don't often feel stress, as I think it is a needless emotion, but this one is easy to get wrapped up in.

Income requirements have been grown accustomed to. Location has been settled into for so many years, that any other prospect seems incredibly daunting. Yet, watching low intelligence decisions being made, money squeezed, processes and red tape being added at an alarming rate... overwhelming and daunting.

I think I need to wallflower myself a large extent more than I am at work. My mouth gets the best of me. I just have these screaming thoughts that the old ways were cleaner. We got more done with incredibly less required overhead. Fun went out the window years ago. Now fun is something I have to manufacture by ignoring my job while I'm at my job.

Shakepeare famously said "kill all the lawyers." I think we should kill all the accountants instead. Better yet, burn it all down and start over. My phlog's subtitle has been "cromagnon had it right, civilization is a scourge" for quite a while. So spot on.
