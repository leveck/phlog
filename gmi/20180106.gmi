# On software (reply to Solderpunk) and laptops

Solderpunk writes[0] about his favorite software. There is some gold in this phost. I was very gung ho about the terminal. Still am. But I will agree with him on the mail client. I still use mutt, but I have Claws on RPoJ and K-9 on my Note Edge. The reasons are identical to Solderpunk's -- sometimes you get pictures or attachments.

Solderpunk also highlighted a tool called hnb[1]. I am going to have to check this out.

In other news, I have acquired a thinkpad x201 with a broken screen and some other issues. I already bought a screen for it on ebay. The total cost to make this 100% will be $55.00. The FreeBSD compatibility list has this laptop as totally compatible. I am pretty stoked to get this project under way. It was an oddly fortunate occurance to come across it like I did.

### UPDATE

```
===>>> All dependencies are up to date
===> Cleaning for hnb-1.9.17_1 
===> hnb-1.9.17_1 is only for i386, while you are running aarch64. 
*** Error code 1
Stop. make: stopped in /usr/ports/editors/hnb
===>>> make build failed for editors/hnb Aborting update

```

well... it is a good thing I have that laptop project.

* [0][gopher://sdf.org/0/users/solderpunk/phlog/my-favourite-software.txt](gopher://sdf.org/0/users/solderpunk/phlog/my-favourite-software.txt)
* [1][https://github.com/gamaral/hnb](
=> https://github.com/gamaral/hnb
)
