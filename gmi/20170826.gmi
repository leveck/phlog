# Omnibook 800ct Project Update

Recieved the OmniBook 800ct I bought from eBay. It is a pentium 133mHz with 80MB of RAM, and 1.43GB 2.5 inch IDE internal hard drive, external floppy drive, and 2 PCMCIA sockets. This laptop was shipped to me running Windows 95B. I attemped to install a 32GB SSD to no avail due to BIOS limitations. I first attempted to install FreeBSD, but it didn't work out. I later found a spring had fallen into the top PCMCIA socket. This was causing the system to hang and likely prevented me from installing FreeBSD.

By the time I had figured this out, I had already made the floppies to install Debian 6. My 3com 589 series PCMCIA NIC was recognized and I was able to complete the install -- with much trial and error. I ended up with a functional command prompt only install of Debian. When I built my house, I did not have cat5 run. So being tethered is not ideal.

For this project, I purchased a Linksys wifi PCMCIA card. It uses a Broadcom b43xx chip which means I had to use ndiswrapper to make it work. Recompiling the kernel did not take as long as I thought it would take. I have never had to rely soley on command line utils for wifi connectivity before, and am still working thru getting it to work consistently. I have been able to surf with lynx and install software with it, however. I will just need to work out the right combination of commands that work without the luck factor (/etc/netork/interfaces? iwconfig essid? wpa_supplicant? all the above?).

I also got XFCE working but do not have any usefull X11 software installed. It was a goal nonetheless.

I have another Raspberry Pi 3 coming. I had wanted to use this laptop primarily as an SSH terminal for the two Pi's. After I work out CLI wifi, I think it'll work like a champ.
