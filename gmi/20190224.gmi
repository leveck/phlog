# Changing schedule

### Entered on Sony PEG-UX50

### 20190224

I am moving into a new role at work. Being in charge of  6Sigma related things instead of people. This is a five day a week role instead of 9 days on and five off. I have one last trip to North Dakota to do then I should not need to  go again (unless the new job dictates I need to of course). I feel some trepidation as I have had my current role since  2012 and my bosses are not on board with this move ~ they were more or less made to go along with it. All in all tho, I also feel excited to do something new as I have been  seriously bored.

In other new, I power cycled the UX50 and the sound went  away. I guess I did not lose a capacitor after all.
