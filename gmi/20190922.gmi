# RE: visiblink - I'm going thru your stuff

### Entered in vim on RPoJ via ssh/iPad/BT Keyboard

### While binging on classic Iron Maiden on shuffle

### 20190922

So, my recent 90 percent of gopher post generated  some conversation
=> gopher://zaibatsu.circumlunar.space:70/0/~solderpunk/phlog/gopherspace-the-tip-is-the-best.txt ^1
,
=> gopher://zaibatsu.circumlunar.space:70/0/~visiblink/phlog/20190918 ^2
,
=> gopher://zaibatsu.circumlunar.space/0/~tfurrows/phlog/2019-09-20_wideGopherSpace.txt ^4
. I understand the  objection of Solderpunk in that seemingly the  phlogosphere
=> gopher://gopher.black/1/moku-pona/ ^5
is the most interesting and  engaging part of what is going on in gopherspace. However, Visiblink summed up quite nicely in his "I'm going to look through all your stuff when  you're not here." post
=> gopher://zaibatsu.circumlunar.space/0/~visiblink/phlog/20190921 ^3
what I was going for. Well, in hindsight I'd like to believe so.  Actually he expressed so nicely another side effect of surfing wider gopherspace. That we can gain insider knowledge of the person who's  gopher hole it is, by essentially going thru their stuff while they're not home. I like that idea a lot. Also, it is so very true. Spartan or cluttered. Hobbyist or workaholic. Politico, philosopher, introvert or extrovert, tech savvy or noob, what music they like, the facets are  numerous.

"Yes, but you got that picture from the  individual's phlog." Some of it. Digging thru their sock drawer adds depth to the picture. Also, true, some gopherholes are very  antiseptic in their content. Your basic links  to floodgap, SDF and the other hubs. For a time it seemed that that was the thing to do in your gophermap. At least that was my impression when I returned to gopherspace in 2017 -- based on surfing! However, that is not the hard and fast  rule I mean, look a 
=> gopher://baud.baby/1/ Cat's gopherhole
, now there is a spot with personality! Or even up one level from the phlog on  
=> gopher://zaibatsu.circumlunar.space/1/~solderpunk Solderpunk's space
-- come on now!

At this point in typing this, Iron Maiden's "The Loneliness of the Long Distance Runner"  started playing. In a melodramatic way it sums  up digging through gopher:

```
Run over stiles, across fields,
Turn to look at who's on your heels,
Way ahead of the field,
The line is getting nearer but do
You want the glory that goes
```

Yeah, the cringe worked so well on the last post that I decided to bring it back...

## Meta::Site

The www is now fully on apache_mod_hoedown
=> https://github.com/kjdev/apache-mod-hoedown ^6
,  instead of apache_mod_markdown
=> https://github.com/hamano/apache-mod-markdown ^7
. Specifically to get footnotes on this post. It just has so many  more and nicer features! I do have an open issue on github for the fact that hoedown does not render gopher links! I will wait a bit and see what  happens before I create a fork... so hopefully the link disapearance on my www is temporary.

### update

I added gopher URL scheme to hoedown/src/autolink.c and this fixed the issue. Apparently, since I always  use git on my own shit and never in collaboration, I have no idea how to send a pull request. embarrassed
