# On restlessness

### 20181126

I have been generally restless for the past little bit. Well let's say a couple months. A general lack of purpose, no motivation for projects, a lot of time spent consuming http. It has even manifested itself in playing video games, which I have not done in years. I hate this.

I have a list of things I want to do to RPoD, a hardware upgrade (larger drive), a tar based rsync backup of the  chroot to an external drive, etc., and instead I do nothing. I hate this.

There is too much sitting around and no action being taken. I need to steel my resolve.

Tags: #motivation #fail
