# Anbernic RG35XXSP XFCE

*Entered: in vim on RG35XXSP*  *Date: 20240929*

## Linux with xorg on a handheld game console

I watched 
=> https://youtu.be/ES3s-WT6FzE Michael MJD's video on RG35XXP-XFCE
and I immediately purchased an Anbernic RG35XXSP. The 
=> https://github.com/MrJackSpade/RG35XXP-XFCE github project
had been updated to resolve some issues shown in the  video prior to me receiving my console. I was able to  install XFCE on the 64 bit aarch64 update to the  device (latest modified firmware available online).

The small screen and partition layout made emacs a no go - stuck at emacs 27 in the ubuntu repos, and didn't care to clear enough space off the device to  compile emacs 29 or 30 after dealing with the v27 install on the small screen. Vim of course works great.

=> gopher://1436.ninja/IPhlog/images/20240929-RG35XXSP-XFCE.jpg the device running lynx, surfing gopher [IMG]

I guess I will see how much use I make on what is  basically a super small laptop. Bluetooth on the device only connects to game controllers, but the USB C pot support OTG, so a wireless keyboard with a touchpad and a 2ghz dongle works great.
