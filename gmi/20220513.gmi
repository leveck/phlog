# Linux on Gemini PDA

Entered: in emacs on gemini pda | Date: 20220513

I had been using my Gemini PDA with un-rooted Android since I purchased it. I think the reason I forgot I even owned this device was that I have plenty of Android devices. There was no reason for me to ever pull out this device. It had no use case.

Then I realized I only used termux on this device. Period. The only app I used was one that simulates Linux. On a device that can actually run Linux. So I decided to reflash my Gemini PDA to only run Debian Linux. I am using DWM, slstatus, emacs 29 (compiled from source), mate terminal (for ease of config), nerd fonts, nmtui (for network), and rclone (for dropbox sync).

You need to completely remove libreoffice before you can apt upgrade or you will be in dependancy hell and apt will not work until you claw your way out. That aside, installing Linux on this device has breathed a new life into it. It now has a purpose. Why did I not do this sooner?
