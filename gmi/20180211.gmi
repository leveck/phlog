# On catching up

I have had some things going on in meatspace. First, my mother's husband died and I drove down to Roswell, New Mexico from Wyoming. I did the trip down (and the trip back) in one marathon drive. I bought a 12 pack of Red Bulls and finished the last one as I pulled into my driveway.

It was so nice to see my mother, it had been a few years. Since my mother got remarried when I was 30, I never considered Robert my step-father, but I enjoyed him as a human. I paid my respects and all in all it was a very positive trip.

When I got back to Wyoming, I had a mental fatigue that comes from breaking routine. Then I found out my son had violated his probation while I was gone. He is sixteen. My wife did not want to weigh me down on my trip so she had kept this from me. My wife, my son, the lawyer, the court, and I all agreed that enough is enough and we asked the court to go ahead and issue a sentence so he can get the help he needs. He was sent to the Wyoming Boy's School. Do not mistake the name, it is juvenile prison. He seems to be doing well though. I hope he makes the best of this circumstance and oportunity.

After this I kind of slumped into a wallflower kind of mode. I have been reading books and researching nutrition. My wife and I have decided to cut out carbs and sugars. Tomorrow is the zero day for this change. She is getting ready as I type this phost, we have our list and are going to the grocery store to implement our change. In 2014 I got down to 198 pounds and felt great. Since then I have packed on 35 pounds I do not want, and am looking more importantly to improve my health and longevity in this life.

I have also embarked on a new music project. A three piece. I have never made music with other people. Nothing serious anyways. I am pretty stoked by this development and have purchased a 4 channel audio interface to capture all three of us live. There is an energy that cannot come from single-one-at-a-time recording of parts. None of us have a clear idea of the music we will make. We are all waiting to se what develops. I have been playing, screwing around with ideas to be nice and "warmed up" for this. An example is here [0].

My cosmic nilhilism and politics have been in high gear. This tends to also result in antisocial behavior. Not antisocial in a lawless sense, more a sense of shunning humanity. My views are shared by my bandmates so regardless of if we decide to write songs about flowers and puppies, the time spent playing will feel very comfortable to me.

# Phlog catchup

I am very much liking Kvothe's phlog [1]. I am not yet thru it but there is some good, honest content there.

Pet84rik went to Finland [1]. How cool is that? I lived in Bavaria as a kid, and Europe has a mystical quality to me. The homeland of my forebears. My family is decendent of French, German, Danish, and Scandinavian stock. Though we have been in the United states since the 17th century, I feel a connection with Europe. I feel the same sort of interest pique thinking of Solderpunk and his various travelings. It is a neat thing to be able to do this.

I will need to continue this another time as my wife is ready to go shopping and I can feel her eyes burning into me :P

* [0][https://nprod.co/DanceOfTheFutile.m4a](
=> https://nprod.co/DanceOfTheFutile.m4a
)
* [1][gopher://sdf.org/0/users/kvothe/phlog/](gopher://sdf.org/0/users/kvothe/phlog/)
* [2][gopher://sdf.org/0/users/pet84rik/BLOG/FEB18/Feb05-Finland](gopher://sdf.org/0/users/pet84rik/BLOG/FEB18/Feb05-Finland)
