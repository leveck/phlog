# On some todos for RPoD

* Identify all type 1 and 0 local links with nonstatic content and generate atom feeds

  * sitelog is the first obvious candidate
  * FilesOfInterest and DocumentStore are also on the list
    * Possibly these two should actually be combined into just the DocumentStore...
* Get off my ass and implement a crontab driven rsync backup of RPoD to the external drive

  * seriously, why haven't I done this?
  * possibly rsync RPoD
  * the external drive to my 256GB thumb drive... gonna think on this
  * maybe a third Raspberry Pi (Pi Zero this time?) to act soley as a backup device?
* Fully migrate all music by me into gopherspace. This is primarily for control. Bandcamp is pretty stable, but I would prefer to have a full hierarchical local copy of my discography, it was a lot of work after all...


* Reorganize and streamine the layout of the main gophermap.

  * How long do I want it to be?
  * is this an issue? I enjoy Papa Grex's long scrolling layout, but tx's minimalism is pretty sweet... this will likely continue to be an organic endeavor.
* Identify more sources of content to import into gopherspace

  * I am a big fan of the HackerNews[0] gopherspace
  * MetaFilter's[1] blatent liberal slant turns me off
  * I feel the same about slashdot (which I stopped visiting sometime around 1999 or so) my slashdot2gopher[2] page is pretty halfhearted in any case (lynx dump)
  * I live in the console, so this is more about a personal search than building the RPoD gopherhole
* [0][gopher://hngopher.com/1/](gopher://hngopher.com/1/)


* [1][gopher://gopher.metafilter.com/1/MetaFilter](gopher://gopher.metafilter.com/1/MetaFilter)


* [2][gopher://gopher.leveck.us/0/cgi-bin/slashdot-txt](gopher://gopher.leveck.us/0/cgi-bin/slashdot-txt)
