# Solo RPG - Prep or not to prep

Entered: in vim on x201 | Date: 20200412

Of late I have been exploring ways to improve my role playing in the game with my daughter. For a long time in solo RPG'ing, I relied on tables to randomize absolutely everything. In doing this, one needs only a block of time to play. There is no such thing as prep needed. There were some experiments in over-prepping that failed...

However, I am left unsatisfied of late using this method. Even though we play without a referee, I do steer the game. I think it is time to prep some. I have many ideas on setting and plot. I just need to jot down some notes.

In related news, I have the DriveThruRPG print on demand versions of The Rules Cyclopedia, and the three core AD&D 1e books coming (shipped already, but cheapest slow method). I am thinking I will transition my daughter and my game from White Box to BECMI. I want to solo using AD&D. I wonder how many weeks of waiting I still have left before I get these books?
