# New Palm Device Coming

### Entered on Sony Clie PEG UX50 in CardTXT

### Soundtrack - metal playing on an iPod Video

### 20190810

So, of all the PalmOS devices I owned in the 90s and early 2000s, the one I think most fondly of was the Handspring Treo 90. I had the Treo 270 afterward. Still liked the Treo 90 the best. The form factor is killer. I still had mine when I moved to Wyoming in 2005... lost it sometime between then and now.

Tonight I found one complete in box with everything it came with new. In the pics, it looks new. Having been bit more than once on older devices (how about every single time?), I bought a replacement battery at the same time.

Since the Treo 90 lacks wireless com, I plan on using IR to my UX50 to offload data. The UX50 can FTP to a Raspberry Pi I setup just for this purpose, over a WEP secured (ha!) WiFi network (with mac filtering enabled). I can also sync Yanoff on my UX50 and beam the DB.

Why? Because I remember fondly the sexiness. Because the  Treo 90 is a solid device. Because CardTXT will run on  it. It has a color screen and an SD card slot yet it  runs on a Dragonball CPU. I have a metric shit ton of  software for pre-arm Palms. As an after thought, I need  to also buy a USB power cable for it... done. In this case  Amazon was cheaper than eBay. Having an AC charger only  is definitely not as handy.
