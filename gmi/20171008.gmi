# On Relating

Lately I have given up on relating. No one seems to see what I take for granted to be readily obvious. About how things work, the world at large, simple truths, etc. Thinking back, it took decades for me to figure things out myself. I suppose that I should be more empathetic to those who haven't gotten there yet. Yet, my interest is nearly nill in engaging with others.

I think that my perfect life scenario is something akin to a cabin in the woods with no roads leading in. No humans for miles. Maybe I could write a manifesto or something...

I disagree with people regardless of their politics. The sounds of other people's activity makes me tense. I fully understand why males of many large mammal species go off by themselves. I also feel too young to be the "stay off my lawn" guy, but such it is. I am fortunate that my wife understands me and is quite okay with how I am.

I have picked up a guitar daily for the past couple days. Mostly playing bluesy sludgey doomy types of riffs, setting up a virtual amp with high gain and much distortion (although last night was all about highly primitive black metal). Feels pretty good. Back to work on Tuesday, and having to interact with many humans for nine days. Meh.
