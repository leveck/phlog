# More on rewrites

Entered: in emacs on x201 | Date: 20200101

I am holding off on a gopher rewrite for a bit. Not too terribly long as I am still worried about not having a portable site. A lot of thought will need to go into this. For example, if I compress each of the PDA software packages, how much space will I save?

I think first, I will tackle a WWW rewrite. I am free to script whatever massaging I wish as I have posting automated with Gnu Direvent. I currently have a naked directory listing with apache calling sed to massage some stuff on the listing, and mod_hoedown on the fly translating the individual posts from markdown to HTML. All of this uses the same files as on the gopher site.

The CSS needs to be spruced up, maybe a simple sidebar will be added. Maybe just show current year and links to prior posts by year, defaulting to showing the current post instead of a mile and a half long list (Canadians and Europeans take note: a mile is like an American kilometer).

Thanks for helping me think this out a bit. I don't think anyone actually frequents the web view of this phlog. I mainly want to better utilize my webserver. For portability's sake, maybe relying on a specific setup of apache2 mods is not the best plan going forward.

Well, it seems I have even more to think about now. Dammit.

EDIT: maybe I am over-thinking all of the gopher stuff? My phlog has a git repo. AND SO DOES THE ENTIRE GOPHERHOLE. All 11.5gb of it lives on gitlab and cron git pushes daily. So I can safely worry about it all later. The gopherhole repo is private due to (c) concerns. I may make an effort to fix this in the future.
