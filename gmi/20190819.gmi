# On Plucker

## or, more Palm OS adventures

### entered on a Treo 90 in CardTXT

### 20190819

So, failing to get any variation of Plucker working on my Raspberry Pi, I reluctantly brought out my ThinkPad x201 which runs Devuan. Since I did not want to mess with installing an ancient Python, I instead installed Sunrise XP.

This is a Windows bit of software but runs fine under Wine. I converted all the complete issues of 
=> http://1436.ninja/AstoundingStories/ Astounding Stories from Project Gutenberg
. However, after I had become engrossed in a tale about a cold-ray, the file abruptly ended. Cut off. All of them appeared to be about 1/5 complete.

So today I took my Treo to work, and installed Plucker and Sunrise XP on my work PC. I do indeed like Sunrise XP  better than OEM Plucker. The new files  still cut off. I checked the version of Plucker I was running on my Treo - v1.1. I was able to locate v1.8. Once installed, all my files displayed their full length. Victory!

I have always been a fan of Plucker. The perfect devices for it, IMHO, are Palm OS 4.x color screen devices - Treo 90, Palm m130, color Clies circa 2002. B&W  (okay, green & black) screens do not  encapsulate that peak Plucker timeframe. OS 5 devices had WiFi and did not need an  offline reader.

Today I also received a Treo 90/180 Belkin faux leather and clear plastic case for my device. I remain on the lookout for the official Handspring leather case advertised in the box. In the meantime, this one is not bad.
