# On the work week

I am at work now, on my lunch hour. It is almost over and I haven't a lot of time. I work an 8&6 schedule (eight days on, six days off), but I always work an extra day (9&5). Then on day ten I always use a vacation day. Every other week, all year long. A normal two week period is: 72 hours worked on week one, 36 hours worked on week two (paid 44 hours due to the vacation day). 116 hours per check is a decrease. Before I left the field, I worked that in a one week period. Two weeks on, one week off. I blame my big white beard at 42 years of age on having put in this much work. At least now, having an office, I am out of the weather, and I do not miss out on so much at home.

Oh well, back to it...
