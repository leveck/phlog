# On further messing with the Lifebook P1120

### Entered in vim on Fujitsu Lifebook P1120

### 20190219

I failed to get Windows ME installed on the Fujitsu. Then I installed antiX Linux. It was okay. nothing that made me super pleased. It was slow on the Transmeta Crusoe 800MHz  proc and 256MB RAM. I installed ratpoison on it and that  sped it up, uninstalled the desktop manager -- slim -- and that did more for it.

Today I downloaded openBSD and installed it on a new CF  card in the laptop. It is peppy as hell. The native wifi  works (it did not in Linux, I was using a PCMCIA wifi  card). I told the installer to go ahead and enable the DM. Ctrl+alt+F3 of course lets me go to the console, ctrl+alt+ F5 takes me to X. I added a repo to the system and installed my normal software, git cloned my dotfiles, and I am at home. So far I am liking this OS. I think I'll keep it.
