# Lies, Damned Lies, and Absolutely Everything

Entered: in vim on Samsung Galaxy S21 | Date: 20220221 | Music: "Think for Yourself" by DRI

I am so fucking checked out and so very sure I have been lied to my entire life. At this point I believe absolutely nothing the media, government, education, or medical doctors say. Basically all authority is based on a pile of lies. I don't even question things anymore. I take it for granted it's a lie. Nothing is shocking. Nothing gets a second glance. I want to add that I have thought this since at least 1993.

I am intellectually cognisant that everything probably is not a lie. I am equally cognisant that absolutely everything could be a lie.
