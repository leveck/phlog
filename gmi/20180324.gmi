# On My Game Master Assistant Script

### -=20180324=-

I wrote a dirty little group of scripts to aid in role playing. Suitable for solo or play with a GM. It is downloadable in the GAMES section of RPoD Gopherhole[0]

# What it does

It has a random oracle that gives conditional and absolute answers. These take the form of: Yes!, No!, Yes, but..., and No, but... It also includes a random event generator. This pulls from files containing verbs, adjectives, and nouns. Lastly is includes a names generator. When executed from gme, it lets you select usual names, male fantasy names, or female fantasy names with a d6. If you run it seperately as, for example:

```
namegen 20

```

It will let you select from a d20. The number is arbitrary and you may use what ever die you'd like, but only one.

Here is a "screenshot":

```
$ gme

==== 
GAME MASTER'S ASSISTANT by Nathaniel Leveck, 2018
====

ORACLE 
BAD: No, but... EVEN: Yes, but... GOOD: Yes!

RANDOM EVENT GENERATOR: 
bury polite eagle

NAMES (CHOOSE WITH 1d6) 
USUAL (MIXED GENDERS): 1: Chester 2: Shanika 3: Veronique 4: Onita 5: Lin 6: Bryan
FANTASY (MALE): 1: Dinfar 2: Etdar 3: Thiltran 4: Darkkon 5: Lesphares 6: Erim
FANTASY (FEMALE): 1: Linyah 2: Niraya 3: Synestra 4: Annalyn 5: Gronalyn 6: Sennetta

$ namegen 20 

NAMES (CHOOSE WITH 1d20) 
USUAL (MIXED GENDERS): 1: Genia 2: Cleo 3: Joleen 4: Nisha 5: Damion 
6: Lannie 7: Tennille 8: Frederica 9: Tamie 10: Coralee 11: Julieann 
12: Leilani 13: Genesis 14: Ned 15: Jenny 16: Lynna 17: Janita 
18: Emelina 19: Catrice 20: Jordan

FANTASY (MALE): 1: Laracal 2: Tespar 3: Cardon 4: Cusmirk 5: Ikar 
6: Justal 7: Drit 8: Milo 9: Sothale 10: Lephidiles 11: Atlin 
12: Ocarin 13: Scoth 14: Picon 15: Hecton 16: Madarlon 17: Toma 
18: Tempist 19: Amerdan 20: Madarlon

FANTASY (FEMALE): 1: Zoura 2: Ada 3: Trinsa 4: Tryane 5: Lyna 
6: Sepherene 7: Calene 8: Gessane 9: Annalyn 10: Annalyn 11: Akara 
12: Sevestra 13: Sevestra 14: Helenia 15: Zilka 16: Naria 17: Adorra 
18: Sevestra 19: Urda 20: Asada

```

-|-

* [0][gopher://gopher.leveck.us](gopher://gopher.leveck.us)
