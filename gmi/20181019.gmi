# I have a problem

### 20181019

So, I recently rediscovered my Palm OS addiction. Since then I have been doing some shopping...

It started with the 8mb handspring backup module. Then an eyemodule2 camera for my Visor Deluxe. Then another Visor Deluxe. Then a stowaway keyboard for the Visor Deluxe. Then a Palm IIIxe, and a gotype  keyboard for it. Then a Sony Clie PEG UX50, and a memorystick duo + an adaptor, and I made an offer on an extended battery for it.

All the stuff I either did own or wanted to in my 20s... it feels like 2000 again, minus a shitty beige box Win2k computer (I did run a Red  Hat box back then too, but never did anything w/ my PDAs on it -- it  was my server connected to my ISDN router). I prefer the gotype  keyboard to the stowaway, I have owned them both in the past. I used a gotype and a trgpro in college... also a treo 90 with an sdio vga card for presentaions, a Palm IIIe, Palm m105, Palm m125, LifeDrive, a couple WinCE Pocket PCs (didn't dig them as much).

Yeah, I have a problem. I am glad my wife gets me, it could definately stress someone who didn't...
