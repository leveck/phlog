# On the joy of simplicty

### -=20180616=-

## Simplification of format

My ph/blogging workflow had taken on a life of its own. Compiling the text. A very rigid format that would be cool if I were writing a newsletter and posting it to a BBS in 1988... multiplexing the ph/blog on 
=> https://tools.ietf.org/html/rfc1436 1436
and the world wide web led me to make a choice. The rigidity had to go. I am still working on the full blown migration of the gopherhole to feed off the same vimwiki as the 
=> https://leveck.us website
. In the end I will have a collection of durable plaintext files and an easy backup plan for my data. Part of that is git, part of that is an Internet Archive mirror. I already have an 
=> https://archive.org/details/thenamtaraumaudiocollection audio
and 
=> https://archive.org/details/thenamtaraumcollection video
collection on the IA. That 
=> https://alexschroeder.ch/wiki/2018-06-06_Self_Hosting alexschroeder phost
really did set off a bunch of thinking and I am going to reverse what I 
=> https://leveck.us/20180606.html already wrote
...

## Simplification of my servers

I have decomissioned RPoP (the Raspberry Pi of Perdition) and was left with RPoD (the Raspberry Pi of Death) and RPoJ (the Raspberry of Judgement). RPoD is the main internet facing server. It runs vanilla Raspbian Stretch, apache and gophernicus. RPoJ was my primary PC. It runs FreeBSD 12.0 Current. Running 12.0 current was a mistake on a Raspberry Pi. So much broken shit. Permissions system wide were always screwing up. I knew, for example that after any pkg upgrade I would be locked out of my user account since it would loose permission to run bash. So, I would have to climb a ladder, connect a monitor and keyboard, login as root, and run my fixall script -- basically chmod 0755 on /usr/bin, /usr/local/bin, /usr/lib, /usr/local/lib, then fix sudo and other items that don't like 0755. Any time I would sudo pkg install anything new, I'd have to run my script before I could run the new app or even see it's man page. The final straw was after my last upgrade, after I unlocked my account, git no longer worked due to a problem with pam.

The solutions are all ubuntu-centric on Stack Overflow. Fuck it. I am done.

So, I have brought RPoA (the Raspberry Pi of Anguish) back out of the drawer it was in and installed stock Raspbian onto a 64GB uSD card. Smooth like butter. There is something to be be said for that. I want to use my computers, not screw with quirks in my OS anytime I want to install software.

While I was setting up RPoA, I discovered byobu. Soon I realized that byobu + screen = pure sex. All of a sudden, I found myself in love with the world, so there was only one thing that I could do was ding a ding dang my dang a long ling long (apologies to those of you who don't know Jesus Build My Hotrod). I did have to change some of my vim mappings to use leader + letter instead of function keys but I was planning on doing that anyways. I do like the look and functionality of this program. Pretty slick. I have spent the day on RPoA seding the ever living crap out of my ph/blog files. Followed by building them into another vimwiki and integrating that into the main one.

I still have not decomissioned RPoJ, but once I figure I have everything off of it that was not git push'd then I shall. It'll be back sometime in the future, as will RPoP -- once a project appears. And they'll both have the current vanilla Raspbian install on their brand new uSD cards. No reason to use a drive that has already had a portion or its finite life used up...

## Simplification of methodolgy

I have also been attempting to deliberately stop myself from overthinking. This I have been implementing for about five years now. No elaborate theories. Only things I can measure, observe, or determine through a verifiable means. An incredible reduction in bullshit has resulted from this. The world is a simple place. Life is good.
