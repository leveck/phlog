# On uptime

My gopherhole is hosted on a raspberry pi, ziptied to a wire cage (to deter cats from messing with the pi's), nestled atop a bookshelf in my living room, plugged into an ethernet port on my home DSL router. I do not have my 3 active raspberry pi's plugged into a UPS, although it is on my list of things to purchase, yet I have fairly decent uptime on my systems. This is due largely to the fact that I am the sole user, and have full control over what is run on the computers.

I have noticed quite a bit of downtime on SDF over the past weeks. Most likely due to the fact that there is a crap ton of users and software running on this public access computer. I utilize SDF daily, but today I took a step toward protecting access to my data.

My phlog script opens two files in tabs in vim, one is the phlog entry, one is references. This is the method I chose so that when par justifies the phlog entry, it leaves the references intact. Once vim is closed, the two files are concatenated. The unified file is then copied to SDF, a directory on RPoJ, a directory on RPoD's external drive, and to /var/gopher/phlog on RPoD. Then it runs the atom feed script via ssh and updates that (the feed is available via https and gopher). The /var/gopher directory is also a git repo that syncs to yet another location.

Since RPoD already has a copy of the phlog locally, I have pointed the gophermap link on gopher.leveck.us to the RPoD copy instead of the SDF copy. I want to maintain the SDF copy since the SDF phlog listing is the largest of it's kind and bongusta! also points to the copy on SDF. The end result is I am hopefully insulated against losing anything or being down due to some action or happenstance I had no part in.
