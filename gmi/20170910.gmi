# On odd forum bugs and laptops

So, according to xargs on RPoP (the living pi), variables can be rather large.

```
echo | xargs --show-limits 
Your environment variables take up 2769 bytes 
POSIX upper limit on argument length (this system): 2092335 
POSIX smallest allowable upper limit on argument length (all systems): 4096 
Maximum length of command we could actually use: 2089566 
Size of command buffer we are actually using: 131072

```

My testing bears this out. Yet, I was unable to post to the forum today, just the date got written to the file without the rest of the actual post. Some one else had the same issue on the politics board afterward. Then I tested it from lynx and my proxy and both test posts (since deleted) were successful. IDK WTF. I think I am going to migrate the forum to mySQL and ditch the apparently flakey appending to text file. This will become a thing after the second RPoD arrives tomorrow.

Had some fun yesterday installing Arch Linux on a HP Stream 14" laptop. I was in IT in the 90s and early 2000s as a technician and programmer. I have been devoting my attentions to other things for so long that a new booting technology -- EFI has come into being and yesterday was my first forray. I only had to install Arch once, but my boot partition was wiped out about six times while I figured this out.

Overall I am impressed with EFI as a technology (my laptop boots in seconds), and very impressed with Arch. There is something so silky sexy about the complete and utter lack of bloat acheived by a clean install using nothing but a command prompt. Current setup is CLI boot, command line WiFi (a skillset I picked up with the OB800 project), xorg with i3, saved layout of terminals, w3m as my main browser (in terminal image support), ranger as file manager, lynx for gopher.
