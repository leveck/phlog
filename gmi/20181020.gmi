# Re: Jirka (UX50)

### 2018.10.20

In response to 
=> gopher://sdf.org/0/users/jirka/Phlog/2018_10_06.txt 2018.10.20 Phost

I am  aware of the shortcomings  of the Clie PEG-UX50...  but I always wanted one,  and now  the price  is tolerable :^)  I expect  myself to either stay with the Handspring Visor  Deluxe or the Palm IIIxe (which will arrive next week while I am  in North Dakota for business). I can have a high threshold for anoyance if a device fits the right niche of nostalgia and function... but I have  an extra soft-spot for the green and black screen devices of my early 20s.

In  other news,  I am  typing this  phost on  my Visor  Deluxe with  a stowaway  keyboard. Stupid  me forgot  there  was a  driver (which  of course did  not come with  the keyboard I bought  on ebay) but  it was easily findable and now lives in my palm programs store on RPoD.
