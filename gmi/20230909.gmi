# docker progress

*Entered: in emacs on ThinkCentre of Death*  *Date: 20230909*

## slow progress, but progress

I have been experimenting with docker, slowly, as time allows. I setup a Mac Mini (momt - Mac of Miniature Torment) up on the shelf where TCoD (ThinkCentre of Death) and RPoJ (Raspberry Pi of Judgement) live. Its sole reason for existance is to be my screw around machine.

I have made my own docker image (a work in progress) based on the Alpine Linux image that everyone uses. I have gophernicus, par, and recutils compiling via RUN commands in my dockerfile. Really that's all I need for the cgi-scripts I have live. My issue now is the log file. I want to point the docker container at the existing log file (/var/logs/gopher/log) and have it continue to update. I am mapping this via:

```
docker run -v ~/log/gopher:/var/log/gopher \
           -v ~/gopher:/var/gopher \
           -e LOG_FILE='/var/log/gopher/log ...
```

I create a user in the docker file to run gophernicus:

```
RUN addgroup \
    --gid 1001 \
    --system \
    gopher
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "$(pwd)" \
    --ingroup gopher \
    --no-create-home \
    --uid 1001 \
    --system \
    gopher
```

In my entrypoint script (stolen from another docker image for gophernicus) I have:

```
if [ ${LOG_FILE} ]; then
                SERVER_ARGS="${SERVER_ARGS} -l ${LOG_FILE}"
                mkdir -p "$(dirname ${LOG_FILE})"
                touch "${LOG_FILE}"
                chown 1001.1001 "${LOG_FILE}"
fi
```

But the log file does not get updated. I will keep plugging away at it. The RPoD guestbook is functional running in container, so file writing is working in general. The UID does not match my user on momt, but I am fairly sure I originally had that matching and it still wasn't working.

In any case, it has been fun messing with this.
