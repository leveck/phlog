# Inspiration

### 20180923

So, while I am waiting for isorespin.sh to remix a Linux Mint 18.3 ISO for the Windows 10 Atom 2-in-1 (a cheap surface clone from  WalMart), I have been reading 
=> https://jirkasnotes.wordpress.com jirka's blog
. It has inspired me to dig through my closet to look for Palm OS  devices. I have a Livedrive (hate it), a Zire (hate it), and a  Handspring Visor (actually quite fond of this one). It has PalmOS 3.1H2 installed on it. I cannot locate the cable for it, so I  visited ebay and found one for $8 including shipping. I have a use for a non-internet connected device and there are still apps online to install on this thing. I am planning on buying some Visor modules for it, but only if I consistently use it... we'll see.

I used to write apps in TinyC right on this device and I still have  a copy of this app, the resource editor, and the on Palm editor on my lifedrive. The lifedrive has nonvolitile storage (of course, it  has an actual spinning hardisk!), so unlike the contents of the Visor which are lost, the lifedrive lives on. Expected delivery of the  cable is 20181001 so we shall see.

The isorespin is complete and I am about to dd it to a USB drive... wish me luck!
