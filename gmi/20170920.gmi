# On the Project Gutenberg Gopher Frontend

The Project Gutenberg Gopher Frontend is currently running on my server as a demo to PG for their approval. If you check it out, please do not hammer their server as this will cause an auto-IP block of my server.

1PG Gopher Frontend Demo /cgi-bin/pg gopher.leveck.us 70
