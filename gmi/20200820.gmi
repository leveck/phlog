# Studio PC

Entered: in vim on studio PC | Date: 20200820

A couple of days ago I setup an older tablet PC (Intel Core i5 2.2 GHz, 4GB RAM) in my studio room with a 43" flat panel monitor. I connected this to a 1970s vintage Zenith C440W receiver for audio output. While I was doing this, I took the time to cleanup the clutter that had built up in my studio.

Unlike my Thinkpad x201 which runs Gentoo, this computer runs vanilla Linux Mint. With a wireless keyboard that includes a builtin touchpad, sitting in the comfy chair in my studio, which is against the opposite wall, and using this computer, is a comfortable endeavor. I am typing this right now while listening to a podcast, with my feet propped up. Nice.

I may install Ardour and other audio software onto this PC since it would make sense based upon where it resides. I have a powered USB hub that is connected to a couple audio interfaces, my drum kit, and a gaggle of other equipment. Sounds like a nice project for this week.
