# On my favorite software

Jandal writes[0] of favorite software and the unix philosophy. Here is some of mine: my phlog script on SDF is a script I wrote using bash/ls/awk/sed. Vim is the one true editor. I have been using it for twenty years... the program just feels good. Lynx is my browser of choice. Bash is my chosen shell. I have tried others, only to find myself borne again. I was introduced to a delicious feed reader called ridi[1] by its author, Rico. Super fast and lean using a shell script to harvest feeds, a parser written in c, fold, and less... I was convinced I was going to write my own and have decided that I just really like ridi.

Mutt for email, ssh/scp constantly, hosts of scripts and aliases, stow, git (still not using this to its potential). So far, I very much like FreeBSD as an OS, when I acquire the Raspberry Pi of Anguish soon, it will have FreeBSD on it as well. A friend is hooking me up with a case with an integrated 3.5" touchscreen. I am still thinking of things to do with that. I am weighing the options of ratpoison and some sort of network monitor visual (with bt keyboard) versus a console with a largish font. I am also considering an 18650 battery modification to run this pi as a mobile device. Maybe a flat formfactor lithium battery instead. Who knows what it will end up being, time will tell.

I will be looking at other minimalist ways of reaching my needs and goals. I am not quite to the point of curling all my surfing thru html2txt... but that does sound kinda cool.

*UPDATE*: well that worked better than I thought...

    [leveck@rpoj.leveck.us][~] $> cat bin/q 
    
    #!/usr/bin/env bash 
    curl -s $1 | html2text -width 67 -style pretty | less

URL copy/pasted from ridi to the script above... that's just sexy

* [0][gopher://grex.org/0/%7ejandal/phlog/favourite-programs-or-not](gopher://grex.org/0/%7ejandal/phlog/favourite-programs-or-not)
* [1][gopher://rico.sytes.net/1/english/software/ridi/](gopher://rico.sytes.net/1/english/software/ridi/)
