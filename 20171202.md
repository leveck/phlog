# On life enrichment thru lurking

So, I lurk. I am not a super chatty guy. I try -- occassionally I will pop onto irc... and then I lurk. Or worse, I find that I have no interest in what is being talked about. The trick is to absorb data you feel a connection with, of course. To pull this off, you need to locate a subset of humans that you find interesting.

So far, the persons in gopherspace are the types of people doing things that I find interesting. I may not even know that I would be interested in a particular subject, yet, on the mention from these interesting people, I will give it a try. I discovered tmux this way[0], started looking at sliderules this way[1], have found that I am not alone in thinking certain ways (the long involved simplifying life thread), discovered markdown this way[2], sdf as an entity has triggered an interest in BSD, etc.

Lurking is the digital equivalent of people watching. Without the danger of someone getting upset with your staring.

* [0][gopher://sdf.org/1/users/tomasino/phlog/20171104-keyboards-and-clipboards](gopher://sdf.org/1/users/tomasino/phlog/20171104-keyboards-and-clipboards)
* [1][gopher://sdf.org/1/users/papa/slidrl](gopher://sdf.org/1/users/papa/slidrl)
* [2][gopher://sdf.org/0/users/dbucklin/posts/2017-09-17-information-gathering-management.markdown](gopher://sdf.org/0/users/dbucklin/posts/2017-09-17-information-gathering-management.markdown)
