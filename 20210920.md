# Mobile Solo RPG

*Entered: in vim on gemini pda* |
*Date: 20210920*

I have been thinking of coming up with a compact portable kit for
solo RPG to take on the road. Towards this end I have purchased 2
decks of PACS (Pocket Adventure Card System) which is an RPG system
in a deck of cards (search PACS on drivethrurpg). I already have the
Gamemaster's Apprentice deck and two decks of dungeon/cave and urban
maps for locations. The GMA deck can replace dice (or coins in the
case of PACS), and the location decks will give a random location.

I will give this a try once the PACS decks arrive and make a post
about how I like it.

