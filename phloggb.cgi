#!/bin/bash
QS="$2"
nof=$(date +%Y-%m-%d\ %R)
if [ ! -z "$QS" ]; then
  #if [[ ! -z $(printf "$QS" | grep "20[0-9][0-9][0-9][0-9][0-9][0-9]") ]]; then
      lin=$(printf "%s" "$QS - $nof" | sed "s/'/\\'/g" | sed 's/"/\"/g' | sed "s@+@ @g;s@%@\\\\x@g" | xargs -0 printf "%b")
      cd /var/gopher/Phlog/
      printf "%b" "\n$lin" > /tmp/t && tac /tmp/t >> gb
  #else
  #    printf "You did not reference a phost! I do not know what to do with your comment.\nPlace the date code of a phost in your comment (eg. 20180706).\n" | gopherize
  #fi
fi
cd /var/gopher/Phlog
cat gbhead
tac gb | gopherize
