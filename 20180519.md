# On a mobile pencil and paper gaming setup
### -=20180519=-

I have been constructing a mobile setup for solo RPG play. The goal is to not use electronics, stay small, not be messy nor distracting. The components of my setup are as follows: A blank sheet 3.5x5.5 Moleskine notebook[0], a Staedtler plastic stencil (geometric shapes)[1] cut down to fit in the Moleskine's pocket used to make hexes and other shapes, a distilled version of the deluxe Tunnels and Trolls rulebook[2] I manually compiled out of my most referenced pages and turned into a pocketmod[3][4] (manually, I do not use a Windows PC), a 1989 Mobile Yahtzee dice roller[5] (keeps dice from going everywhere, anything over 5d6 I am hard setting to 3.5 for ease of use), and a Faber Castell perfect pencil[6].

The goal of this rig is a go anywhere, small time taken, immagination boosting experience. I am hand drawing maps and tables in the Moleskine. Items such as this that will be referred to a lot, I am laminating in situ with clear self adhessive contact paper. The Mobile Yahtzee dice roller is silently rubbed across your thigh and is small and portable. I have still in my prep phase but I have adopted the "everything related to play IS play" viewpoint. I find myself drifting into daydreams about the game world. This is a rather large departure for me, and I quite like it.

-|-

* [photo of the mobile setup](https://leveck.us/blog/mobile-rpg.jpg)
* [0][https://www.amazon.com/dp/8883701038/ref=cm_sw_r_cp_apu_i_INgaBb9M32S1N](https://www.amazon.com/dp/8883701038/ref=cm_sw_r_cp_apu_i_INgaBb9M32S1N)
* [1][https://www.amazon.com/dp/B00AETPW6Y/ref=cm_sw_r_cp_tai_i_aQgaBb3ZJWEZZ](https://www.amazon.com/dp/B00AETPW6Y/ref=cm_sw_r_cp_tai_i_aQgaBb3ZJWEZZ)
* [2][https://www.amazon.com/dp/094024490X/ref=cm_sw_r_cp_tai_i_kUgaBb7VZZ9S2](https://www.amazon.com/dp/094024490X/ref=cm_sw_r_cp_tai_i_kUgaBb7VZZ9S2)
* [3][http://pocketmod.com/](http://pocketmod.com/)
* [4][https://leveck.us/tntpocketmod.pdf](https://leveck.us/tntpocketmod.pdf)
* [5][https://www.ebay.com/sch/i.html?_from=R40&_trksid=p2380057.m570.l1313.TR0.TRC0.H0.X1989+mobile+yahtzee.TRS0&_nkw=1989+mobile+yahtzee&_sacat=0](https://www.ebay.com/sch/i.html?_from=R40&_trksid=p2380057.m570.l1313.TR0.TRC0.H0.X1989+mobile+yahtzee.TRS0&_nkw=1989+mobile+yahtzee&_sacat=0)
* [6][https://www.amazon.com/dp/B000KT8RSI/ref=cm_sw_r_cp_tai_i_TZgaBb12BA12R](https://www.amazon.com/dp/B000KT8RSI/ref=cm_sw_r_cp_tai_i_TZgaBb12BA12R)
