# On music and weather

Last night after a Burzum binge, I crafted a nifty ambient track using a faithful simulation of a VCS3 synthesizer. I am pretty stoked about this, and may turn it into a project.

Prior to that, as I drove home from work, it started to snow. It snowed all night. It is snowing right now. Rage of this morning: people driving 20MPH because there is snow on the ground, while it is fresh and not packed, no ice, on a straight, well lit road. Fucking A, stop being so timid.

