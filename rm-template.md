# RPoD Phlog

My personal phlog (gopher-log, like a blog but on gopher instead of the www).

## Available Online Locations

* gopher://1436.ninja/1Phlog
* https://leveck.us/1436.ninja/1Phlog
* finger phlog@1436.ninja
* rss feed from either gopher or http

