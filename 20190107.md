# The projects are rolling in
### Entered in vim on RPoJ via ssh
### 20190107

I have an idea for a novel I want to write on my Alphasmart
Dana. This is currently in the form of an incomplete 
outline. A lot of historical research needs done. I see some
time at the local library in my future.

In addition to that, my band, _[Telerumination][1]_, has 
started working on our fourth album. I need to spend some 
time with a guitar and work on melody and riff. I am taking 
this one slowly.  Liam's ambiant track is dense and mystic. 
The instrumentation layers need to match.

[1]: http://namtaraum.productions/?p=Telerumination "Telerumination Official Webpage"
