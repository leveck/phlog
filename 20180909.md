# On the www
## or Shit sucks, man
### 20180909

I took some big chances today. I moved RPoD gopherhole to the 
chrooted gosher installation. Took some iptables changes (routing
7070 to 70) and some faith. Tell me if stuff is broken, I found a 
bunch already left over from testing (I changed a lot of directory
structure, etc.) -- leveck (at) leveck (dot) us.

I also deleted my website. I felt some trepedation with having my 
phlog on clear-net. It all just felt too exposed. Also along with 
that is my general hatred of the web. Javascript divs covering 
content, ads, clickbait, malware, coin-miners, etc. Screw the entire
mess! I like my subsection of sites such as full-chan, and some 
others. Stepping outside my vetted bookmarks is an exercise in 
frustration. I fully remember web 1.0 and how it used to be before 
browers became a javascript vm and everything went to christian hell.

I think I will onion-ify my gopherhole in then next month or so...
I need to see if I can get an addy containing rpod. I have nothing 
but time and no hurry.

I am also screwing around in C off and on, my goal here is to 
become better at planning and algorthms instead of shooting from the
hip.
