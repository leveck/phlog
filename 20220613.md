# Progress in configuration

*Entered: in emacs on gemini pda* | 
*Date: 20220613*

## stumpwm modeline

I wrote a simple script that displays current window 
title, wifi SSID, CPU temp, battery percentage, date and 
time. I have the TTF font stumpwm module and nerd fonts
insalled so I have icons in the script output. As an
added feature if the battery status checks to other 
than *charging* the battery icon changes to pink as 
an extra indication the battery is discharging. I'll
eventually do the same with the CPU temp.

## backup plan

I have my dot files, org files, and selected items from
my builds directory backed up to dropbox via rclone.
I wrote a script to install all currently installed
packages from dpkg and guix so I can redo faster if 
somthing bad happens (again).

