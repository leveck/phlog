# On PIM (RE: jirka and xiled)
### Entered in vim on RPoJ
### 20190930

Recently xiled wrote a reply[^1] to jirka[^2]
about PIM software.

Now, I have never been a huge user of the PIM
software that came on the Palm. However, as I
have mentioned[^3], I have taken steps to use
the ToDo list and other portions.

I have also been (slowly) reading Getting Things
Done[^4] in an attempt to become more organized.
I find myself in the same sort of exploratory
situation as xiled of coming up with a system
to use. I already carry a Treo 90 everyday, so
I am planning on this nebulous future system
utilizing this device. I do use the *myNotes*
functionality of PeditPro[^5] to keep a journal.
I was formerly using PalmWiki[^6], but switched to
a running file format as an experiment. I am open
to going back to PalmWiki in the end if I 
determine it to be the best option.

[^1]: <gopher://sdf.org/1/users/xiled/phlog/2019/20190929_car_wash>
[^2]: <gopher://sdf.org/0/users/jirka/phlog/2019_09_16.txt>
[^3]: <gopher://1436.ninja/0/Twitpher/YDPz-Remind_Script_Palm_OS_ToDoDB_Integration>
[^4]: <gopher://1436.ninja/9/Palm/Palm_Doc/Getting_Thing_Done.pdb>
[^5]: <https://people.math.osu.edu/nevai.1/PC/palm/>
[^6]: <https://web.archive.org/web/20041016045348/http://pitecan.com/PalmWiki/>
