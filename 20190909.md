# Shunning
### Entered on Treo 90
### 20190909

I have written before, if memory serves,
about a complete loss of fitting in with
modern life. Being as how I am currently
typing on a 17 year old, noninternet
connected PDA, I'll forego a link... Any
way, this has progressed. At work, I
write jquery code for sharepoint sites
as part of my job. At home anything
to do with that seems foreign. The slick
appearance of the modern web seems
aesthetically incorrect. The slick metal
and glass, large form factor phones
everyone carries seem like a pocket bulge 
full of radiation and extra weight.

I do use an iPad Pro as my main device 
when home. However, this is primarily
(by a huge margin) used as a Linux 
ssh terminal, and for music production
(damn near zero audio latency on an
iPad). When I leave the house, I leave
my phone at home. If I do surf the web 
it is to use startpage to search for a
snippet of sed regex, or a httpd config
option. Usenet is another huge time
sink for me. The protocol appeals to me
-- a forum that predates the internet
and existed without it. 

I was on a dead-tree book binge before
the last round of PDA purchases, but
have been indulging in Project Gutenberg
converted to Plucker. Cheaper and 
satisfying on two different fronts to me.
Also, to those of a like mind to me: on
RPoD Gopherhole there lives a local
version of the Project Gutenberg in
Gopherspace. Unlike the version I wrote
for PG that they host, the RPoD version
converts books on the fly to Palm Doc
in addition to plaintext. Look for it in
Apps section of RPoD...

My love for plaintext has made me 
immune to wanting flashy apps. Absolutely
any old device will do for my purposes.
(now that I have said that, I need to
write a phlog post as a set of BASIC REM
statements on my Sharp PC1261, save
it to cassette and figure out how to
read that on a Raspberry Pi...
