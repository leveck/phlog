# On gaming progress
### -=20180419=-


So, while I was North Dakota I had planned on delving into my imagination-building-through-RPG project. It turned out I was just too tired to carry it out. I did a significant amount of reading in furtherance of the goal, however, so it was not a total loss. I am pretty sure I know now what initial path I wish to take in my endeavor.

I initially started with Tunnels & Trolls. I have decided to stick with this. I have the 1st, 5th, and Deluxe edition rule books as PDF files, all printed. Deluxe Tunnels & Trolls has some nice additions IMHO so I have a paperback copy coming. To flesh out solo free-form play, I have bought a copy of Scarlet Heros in paperback. There is a wealth of tables and mechanics from SH that I will be blending with the DT&T rules to end up with a very workable system for less structured solo play.

## Record Keeping

I evaluated many different avenues of record keeping for game play. The most impressive collection of tools by far is that of Alex Schröder[0]. You may be familiar, he's a `local' in these parts. The map generation tools, character sketches, etc. on his site are very well done, quality products (that he gives away, pretty cool).

In the end, however, I have decided to opt for paper. Even after (before hitting Alex's site), I created an all-in vim RPG environment. It features an empty hex map, auto rolls your character, and defines F-keys for dice rolls. All in beautiful ASCII text. But, paper... just seemed more in line with what I am after. I experimented with various hex and graph-paper pdf generators and pre-existing files, but in the end I purchased a hex paper notebook. For note taking I have a tried and true graph paper moleskine which lives in a hand made leather cover I bought years ago.

## Character Building

No personal investment in a character seems to me to be a sure fire way for failure in my goal. To aid in this endeaver, I have converted Ashs Guide to RPG Personality & Background[1], a helpful guide full of tables on creating character backgrounds into text and hosted it on my gopherhole[2]. There are of course many other sources for character background. I have also purchased a number of other systems and tables on DriveThruRPG[3] that accomplish the goal in different ways. There is a difference between hosting purchased products and rehosting public content, however, so I decided to do the latter.

## European Heritage

As a 13th generation (12th on my father's side) American, I suppose there is not a lot of physical connection with Europe for me. I did live in Germany twice as a child during my father's service in the United States Army. My sister had her DNA tested and gave the results to me as the geneological portion would apply to me also. I am of 99.7% European descent, primarily western and northern Europe. This led me into a large research project, although nothing I have organized so far, into my forebear's culture. Much fantasy fiction and games material of course is rooted in native European traditions and beliefs. I feel a connection to the material, and this feeling drives the settings of play. Pretty neat stuff this hobby.

-|-

* [0][https://campaignwiki.org/](https://campaignwiki.org/)
* [1][http://www.ashami.com/rpg/](http://www.ashami.com/rpg/)
* [2][gopher://1436.ninja/0/games/ASH](gopher://1436.ninja/0/games/ASH)
* [3][http://www.drivethrurpg.com/](http://www.drivethrurpg.com/)
