#!/bin/bash
searchTerm="$2"
printf "Searching RPoD Phlog for $searchTerm." | gopherize
egrep --include=\*.post -rEoi '/var/gopher/Phlog/' -e ".{0,40}$searchTerm{0,10}" | sed -E '/^\s$/d;s/\t//g;s/\/var\/gopher\/Phlog\///g;s/^(.*)\:(.*)/0\1:\2\t\/Phlog\/\1\t1436\.ninja\t70\ni \tERR\terr.host\t0/g;s/(\t.*)post:.*\t14/\1post\t14/g' | sed '/i\tERR/d;/\t0/d' | sort -r
#egrep --include=\*.post -rnw './' -e "$searchTerm" | gopherize
