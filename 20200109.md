# More vim fun!

*Entered: in vim on x201* |
*Date: 20200109*

So, I recently spent a week in vanilla emacs. I had fallen in love
with org mode, but could totally do without the foreign atmosphere
of the editor itself. I went back to vim and wiped my ~/.vim and
~/.vimrc to start afresh after all these years. I started with
vimorganizer to give me the most complete org mode experience to be
had in vim. I modified this package to my own needs as detailed in
my [last post][1] so that my agenda was more sane. I also installed
[vim-latex-live-preview][2] as I use LaTeX as my sole word processor.
As it is very useful to have a shell available inside your editor,
I also installed [vimshell][3]. For easy templating of arbitrary
filetype boilerplate, [vim-template][4] is the plugin I have
installed I made some additional templates for custom file types I
use and it works great. There are plenty of times I have wanted to
browse gopher from vim, especially when writing a phlog post. So,
for this, I installed [browser.vim][5]. Now, this will not surf
gopher URLs without modification of the code, but it is trivial to
correct this. For aesthetics I like powerline, but loath python and
its slugishness. So I installed [airline][6] instead. This plugin
also let's you theme the tabline of vim. It is pretty snazzy IMHO.
For a better look while writing markdown (such as this document), I
installed [vim-markdown][7].

Along with plugins, I wrote some functions that do things like open
my org agenda, move the split where I want it, open another tab to
vimshell, etc. My current vim setup is the best I have ever had.

[1]: gopher://1436.ninja/0/Phlog/20200106.md
[2]: https://www.vim.org/scripts/script.php?script_id=4524
[3]: https://github.com/Shougo/vimshell.vim
[4]: https://github.com/aperezdc/vim-template
[5]: https://www.vim.org/scripts/script.php?script_id=2227
[6]: https://github.com/vim-airline/vim-airline
[7]: https://github.com/gabrielelana/vim-markdown

