# UNIX Section

## Intent

This is the section for notes about 
Linux, BSD, GNU, shell, sed, awk, perl,
etc.

## Links to Other Pages

  * [Back to index](/Phlog/gen/index.md)

## UNIX50

Over at sdf[^1] they have virtual
machines running classic Unices that
you can play with from your browser.

## direvent

Seriously, why did I not know about
this before? It is a utility that watches
a directory and runs a command or
script when an event happens. Events
include file creation for example. 
Read the manual linked in this 
footnote[^2].

[^1]: https://unix50.org
[^2]: manual at - https://www.gnu.org.ua/software/direvent/manual/direvent.html

<hr><div style='text-align:right;'>all content &copy; 2019 Nathaniel Leveck unless cited differently. &nbsp;$UPDATED 2019.09.13 &nbsp;![RPoD General Purpose Section is composed on a Palm](rpodgen8831.png)</div>

