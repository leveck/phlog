# RPoD General Purpose Section

![](RPoD-gps.png)[^1]

## Intent

This is the area for content that is not
a phlog phost, but that I want to still
hang on to the data. All content will
originate on a Palm OS device and be
composed in Markdown. Basically as
a manual wiki... since most Palm OS 
wikis required hotsyncing and a conduit
running on a Windows 98 machine --
which I have, but it is airgapped and I
intend to keep it that way. All in all an
ftp / telnet solution is not unmanagable.
See the Palm OS section for details.

## Other Parts of RPoD

  * [Up to Blog](/Phlog/)
  * [RPoD gopher to http proxy](http://jynx.gopherite.org/)

## Links to Other Pages

  * [Palm OS](/Phlog/gen/palm.md)
  * [*NIX](/Phlog/gen/unix.md)

[^1]: Drawn in Palm Notepad, converted to png with pilot-read-notepad on Linux

<hr><div style='text-align:right;'>all content &copy; 2019 Nathaniel Leveck unless cited differently. &nbsp;$UPDATED 2019.09.13 &nbsp;![RPoD General Purpose Section is composed on a Palm](rpodgen8831.png)</div>

