# On old Windows
### Entered on RPoJ in vim via ssh
### 20190214

There is a limit to what you can install old versions of
Microsoft Windows on, hardware-wise. You may be thinking,
"yeah, that's obvious," but the line is close. By that I 
mean that a PC _Designed for Windows XP_ most likely 
contains hardware whose drivers absolutely do not exist for
Windows 9x. This was my painful discovery on the Fujitsu
Lifebook P1120. There are Windows 2000 and Windows XP
drivers for the odd Fujitsu variant of the ATI Rage 
Mobility M video adapter with double the VRAM of the usual 
version (a whopping 8MB!), but no extent Win9x driver.
The actual ATI Rage Mobility M and Mobility M+ driver for
Win9x do not work for the LB1120 under Win9x. Blue 
screens follow if you disregard the warning that you should
not be installing it.

It was at this point that I had the epiphany that I should 
stick with computers released between 1993 and 2000 for 
running Win9x, with the sweet spot being 1996-1999. Any odd
hardware should be avoided. Popular and standard hardware
should be preferred. This is the assemblage of well 
supported items that make life easier. 

With resignation, I installed AntiX Linux on the Fujitsu. I
like the distro so far. It is systemd free, Debian based,
resource frugal, and peppy on the 16 year old mini-laptop I
stuck it on. It plays chocolate-doom, and DOS games in 
emulation very well, so I can do some of what I wanted to
do with it if I were able to slap Windows ME on it and get
it to work. I will eventually try again on a more common
laptop... I already bought the OEM disc and other supplies.

