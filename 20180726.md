# On web as newspeak: limiting features to corral people
### 20180726

People use  the web basically  as an  appliance. It is  an omnipresent
background  item like  a  toaster. Whenever  I am  around  a group  of
people, adults or children, most are on their phones. Nothing profound
is being accomplished,  games being played, text  or instant messages,
social media, etc.

In 1984, the book by George Orwell writes that Ingsoc had created its 
own language, [Newspeak](gopher://gopherpedia.com/0/List of Newspeak words), 
and the vocabulary shrank over time to steer people's thinking.

Today,  corporations accomplish  the  same goal  thru  the removal  of
features  from  products.  Lack of  replacable  batteries,  expandable
memory, lack of  choices for cloud storage, etc. Along  this vein, now
we have Mozilla attempting to futher kill RSS.

Feeds are not  desirable from a corporate perspective  because you are
viewing  their  content outside  of  their  interface --  and  without
advertising. The  end user  experience is very  desirable --  they are
able  to view  the  content  outside of  the  corporate interface  and
without advertising!

The bloated,  cluttered, bullshit interfaces most  large websites have
nowadays are terrible IMHO.  RSS is a way to get  straight to the part
that's actually meaningful. Doubleplusungood...

