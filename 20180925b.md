# re: enforced anonymity [tomasino]
## 20180925b

Reply to [phost here](gopher://gopher.black/1/phlog/20180924-enforced-anonymity)

Anonymity is a holy cornerstone of net culture. It is the root of the
infamous imageboard exclamation: "tits or GTFO". This follows a post
where a person identifies themselves as a female (which is always done
as an appeal for attention or to gain a privlege). In an anonymous 
forum, all participants are sexless, all statements are weighed upon
their merit, no consideration given to the identity of the poster.
This engenders freedom of discourse and encourages well thought out 
posts... ignore /b/...

The thrusting of feelings into something like the kernel is an 
terrible idea. Linus protected the kernel like it was his child. You
knew when someone messed up (and were entertained as well). The code
does the talking -- no one really cares if the code was written by
a person of a particular sex. However, projects that specifically 
only allow homosexuals, racial "minorities", and women as 
participants are the most harmful IMHO. 

Linus' light-switch-like flip to appologetic and his leaving (his 
statement is odd BTW, as it contains smart quotes. An analysis of
his git commits going all the way back shows he has never used them 
except once -- quoting someone else...) almost feels like a gamer-
gate style takedown. A power play. Extortion.
