# Adventures in Plumbing and Other Things

*Entered: in vim on gemini pda* |
*Date: 20210721*

I am at home for a couple days. In my absence, it was determined that
the hose bib for the spigot on the back of my house was bad. Water
was leaking down the foundation when the water was on. As it is
summer and hot, and I have an evaporative cooler, water was left to
run. Today I had a plumber come out. He made quick work of it and
all is well.

## Side Missions

I have been on a mission to find the perfect travel backpack. I have
a lot of them for this reason, but haven't found one that has pleased
me. It dawned on me that I don't want a backpack, I want a wheeled
laptop bag. No more weight on the ole shoulders and all that. So I
have bought a Samsonite roller bag that I *think* will fit under all
plane seats (I'll soon find out). At least it cost me less than the
bag I looked at while in airports which were between $550 and $780.

Tomorrow I am also going to go through this bag, my toolbox, and my
main suitcase and cut contents to see if I can reduce weight. I
know this is entirely doable. I just want to get down to what is
absolutely essential. I just want clothing for a week, because I can
(and do) just do laundry out of town. I have tools in my toolbox I do
not necessarily need. It has increased over time, from 48 pounds to
59 pounds. I get three free checked bags up to 70 pounds, but I want
to optimize this anyways.

## Gemini PDA

I am typing this in vim in termux on my Gemini PDA. So far this device
is everything I envisioned it to be. I have a 2TB dropbox account I
use for everything. One of the things I use it for is my org agenda.
I use orgzly on my android phone connected to my dropbox to show a
widget. I use beorg on my work iphone connected to my dropbox to do
the same.

With dropsync installed on the gemini, I can use emacs in termux and
have a synced copy of my dropbox org directory that propagates
everywhere whatever device I pick up will show me my org agenda and
the proper view it of course on the gemini in emacs.

Being able to use terminal programs to interact with files stored in
my dropbox is a game changer. I am sure I will find other uses as
time goes on. So far I am really liking this device, and plan on
sticking with android as the only OS on it as termux is so damned good
at giving me exactly what I need Linux for. I do not need xorg because
I have... Android. So web browser and any GUI app I may require. The
missing link was always text based software, which is a huge amount
of what I use a computer for.

