(defvar which-device "not set")
(setq which-device (getenv "device"))
(add-hook 'find-file-hook (lambda ()
  (when (and (stringp (file-name-directory buffer-file-name))
             (string-match "phlog" (file-name-directory buffer-file-name))
             (when (and (stringp buffer-file-name)
                   (string-match "\\.md\\'" buffer-file-name))
                   (when (= (buffer-size) 0)
                   (insert
                    "# \n\n*Entered: in emacs on " which-device "* | \n"
                    "*Date: " (format-time-string "%Y%m%d" (current-time))  "*\n\n## "
                    )
             ))))))
