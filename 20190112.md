# Writing Progress
### Entered on Sony PEG-UX50
### 20190112

I spent the day researching for my story, which is set in
ancient Gaul. At some point, I switched to researching
writing itself. This led me to scrap my existing outline
in favor of a 20 chapter mass paperback structure. I feel
it will give my story a more coherent framework. So now I
have a lot of questions to answer about characters and their
motivations. If you don't know your characters, how can you
accurately speak for them?

I want to write using CardTxt on the Alphasmart Dana. For
the prep work, however, I have decided to use AlphaWord on
the Dana. The ease of switching between files made this 
choice. AlphaWord is a rebranded version of WordSmith for
Palm OS (which i actually paid for around 2000, but my reg 
code is long gone). AW does not save to plaintext files. It
uses a modified PalmDoc file format. I have WordSmith on my
PEG-UX50, and can do file conversion there. I want to write
my actual story in plaintext files, thus CardTxt.

Even though it is currently far from November, once I have 
sufficient research completed (next up on the docket: 
Celtic religion in general, Gaulish in particular, and a
comparison with German/Scandinavian and other European 
flavors as well. As my core idea revolves around a meta-
physical happening, I think the time investment is worth 
it.), I would like to set a goal to keep a NaNoWriMo kinda
pace. This may be asking a lot, but with good notes and a
good outline it shouldn't be too difficult. I guess time
will tell if it is a goal I will meet.

