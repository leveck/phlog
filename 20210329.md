# Yet Another Travel Day

*Entered: in vim on my phone* |
*Date: 20210329*

I am currently on a plane going from San Francisco, California to
Sacramento, California. My day started at 0400, it is now 1334 PDT.
When I land I have to rent a car and drive to another town.

Today's airport reading topics were spiritual in nature

* gnostic thought in Ođinnism
* Christian syncretism as preserver of traditional European belief
* Hermeticism as shared root
    * single source vs multiple genesises

Interesting day.

