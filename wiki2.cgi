#!/bin/bash

PHOST=$(printf "$2" | sed 's/\;//g')
MT="i	Err	host.err	0"
printf "%b" "$(head -1 /var/gopher/Phlog/gen/$PHOST.gen | sed "s/^# //g;") ($PHOST)\n" | gopherize
printf "%b" "0Text via Gopher\t/Phlog/gen/$PHOST.gen\t1436.ninja\t70\r\n"
#printf "%b" "hHTML via Gopher\t/Phlog/$PHOST.html\t1436.ninja\t70\r\n"
printf "%b" "hAs HTML via WWW\tURL:https://leveck.us/Phlog/gen/$PHOST.gen\t1436.ninja\t70\r\n"
LINK=$(/var/gopher/getrefs /var/gopher/Phlog/gen/$PHOST.gen)
LLINK=${#LINK}
COM=$(tac /var/gopher/Phlog/gb | grep "$PHOST " | sed "s/$PHOST//g;s/^[ \t]//g;")
LCOM=${#COM}
if [[ "$LLINK" < 1 ]]; then
    #ZERO LINKS
    printf ""
else
	printf "\nLinks Leaving This Phost:\n" | gopherize 
	printf "$LINK\r\n"
fi
if [[ "$LCOM" > 0 ]]; then
    printf "\nComments On This Phost:\n$COM\n" | gopherize
fi
printf "\n" | gopherize
