# Treo 90 Arrived
### Entered in CardTXT on Treo 90
### 20190815

I came home from work this afternoon 
and my new (literally new in box, this
thing still has the protective sticker on
the screen) Handspring Treo 90 was
waiting for me! I have not yet received
the replacement battery, but did also
receive the USB charging cable. I have 
had bad luck in the past with old lithium
batteries, and did not have any faith in
the one in this device, it is 17 years old 
after all.

Well, I plugged it in and it turned on. It
went through its first time setup just
fine. The battery was flat dead, but 
over the next hour it charged to full.
While it was charging, I beamed some
apps to it from my Sony Clie PEG UX50.
First was CardTXT - a text editor that
can save UNIX or DOS EOL text files to
the SD card (I am running a 128MB SD
in this device). Next came UniCMD. This
is a file manager that also does backups
and allows receiving beaming of non-
Palm files. This is crucial.

Next I tried a few IF apps to play Z
machine games. CLIFrotz and Celardoor
both refused to run on this device -
they require a high res screen. Kronos,
however, works like a champ. I beamed
20 z? files to the Treo from the UX50.
UniCMD made this possible.

After that I kind of went crazy... here is
the list of apps installed:

* FileZ
* iLarn
* X-Master
    * Fonthack123 + dozens of fonts
    * Screenhack
    * PalmWiki
    * Grephack
    * Treo Keyboard Utilities (archive.org)
    * Parenthesishack
* OnBoardC + OnBoardASM
* LispMe
* IcoEdit / RsrcEdit / SrcEdit
* Pluckr
* PeditPro (reg'd)

These were beamed either from one of
my Handspring Visor Deluxes, or my
UX50. Some apps were downloaded on
my iPad Pro then FTP'd to the Raspberry
Pi of Anguish then d/l'd to my UX50 then
beamed over to the Treo 90... what a 
good time!

I am so enamoured with tin right now
as an NNTP client, that I do not think I
will use Yanoff on this device at the
moment. Plus I do not want to lose my
place on comp.unix.shell... this device
is like 2002 all over again and I love it.
