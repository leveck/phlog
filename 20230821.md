# Scratch mojo 

*Entered: in emacs on GPD Pocket X7* 
*Date: 20230821*

## Let me explain

I essentially started RPoD over from scratch when I replaced
the Raspberry Pi with a ThinkCentre. This was a change in
architecture, a change in gopher server software, and a
combining of multiple servers into one machine. A lot of
scripts got broken. This included my atom feeds and my main
phlog handling script (handles git, calls the atom scripts,
scps things around, builds gophermap/index.gmi, etc.) and
quite a few other things as well.

But the main phlog script was a pain in the ass because it
basically stopped me from posting. So I rewrote it tonight,
greatly simplified the workflow, and things are smooth.

I haven't wrote that much code in a long while. I kept
things a lot less kludgy (the old scripts became a spaghetti
mess of mile-long one-liners strung together to fix a bug
that was an edge case, and never got cleaned up or
rethought) I am fairly pleased with it.
