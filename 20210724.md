# joneworlds words

*Entered: in vim on gemini pda* |
*Date: 20210724* |
*Soundtrack: Blitzkrieg - Standing Still*

I just wanted to give a nod to [joneworlds][1] for being rad AF. I
hadn't done much gopher exploration for some time, so I perused
several phlogrolls and discovered this gem of a gopherhole. The way
this individual puts words together is something I enjoy quite a lot.

[1]: gopher://republic.circumlunar.space/1/~joneworlds/

