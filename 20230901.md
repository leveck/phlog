# RPoD ideas

*Entered: in emacs on Samsung Galaxy S21+* 
*Date: 20230901*

## redo coming

I am going to be experimenting with docker and containerized
gophernicus. I also really like the idea of GeGoBi as a
gemini server, putting this in docker shouldn't be
difficult. 

The change for the actual gopherhole would be that since
GeGoBi doesn't execute gophermaps chmod'd 0755, it just
serves the naked script that is RPoD's main gophermap (feeds
off a recfile). So I am thinking of writing a site generator
that feeds off the recutils database and spits out static
gophermaps that would also live on port 1965 (gemini).

Are there any other gopher=>gemini servers? GeGoBi hasn't
been touched in a few years, not that it matters since it
does the job. I only ask because if there was another
software package and it executed my scripted gophermap as
is, it'd save me some work.

sysop [at] 1436 [dot] ninja
